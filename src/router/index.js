import { createRouter, createWebHistory } from 'vue-router'
import PersonalDetails from '../views/PersonalDetails.vue'
import CompanyDetails from '../views/CompanyDetails.vue'
import EmailVerification from '../views/EmailVerification.vue'
import AboutView from '../views/AboutView.vue'


const routes = [
  {
    path: '/',
    name: 'PersonalDetails',
    component: PersonalDetails
  },
  {
    path: '/CompanyDetails',
    name: 'CompanyDetails',
    component: CompanyDetails
  },
  {
    path: '/EmailVerification',
    name: 'EmailVerification',
    component: EmailVerification
  },
  {
    path:'/AboutView',
    name:'AboutView',
    component: AboutView
  }

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
