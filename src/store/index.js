import { createStore } from 'vuex'
import axios from 'axios'

export default createStore({
  state: {
      persons:[] ,
      company:[],
      countries:[] 
  },
  mutations:{
    setPerson : (state,persons)=>(state.persons = persons),
    setCompany : (state,company)=>(state.company = company),
    setCountries:(state,countries) => (state.countries = countries),
    removeUser : (state,id) => (state.persons = state.persons.filter(del => del.id !== id)),  
  },
  getters: {
    allPerson: state => state.persons,
    allCompany: state => state.company,
    allCountries: state => state.countries
  },
 
  actions: {
    async addPerson({commit},{name,selectedGender,selectedCountry,selectedState,contact}){
      const response = await axios.post("http://localhost:5600/person",{name,selectedGender,selectedCountry,selectedState,contact})
      console.log(response.data)

      
    },

    async addCompany({commit},{companyName,email,job,experience,checked}){
      const response = await axios.post("http://localhost:5600/company",{companyName,email,job,experience,checked})
      console.log(response.data)

      
    },
    async addEmail({commit},{inputNumber1,inputNumber2,inputNumber3,inputNumber4,inputNumber5}){
      const response = await axios.post("http://localhost:5600/email",{inputNumber1,inputNumber2,inputNumber3,inputNumber4,inputNumber5})
      console.log(response.data)

      
    },
    async getPerson({ commit })
    {
      const response = await axios.get('http://localhost:5600/person')
      commit('setPerson', response.data)
    },
    async getCompany({ commit })
    {
      const response = await axios.get('http://localhost:5600/company')
      commit('setCompany', response.data)
    },
    async updPerson({ commit }, updPerson)
    {
      const response = await axios.put(`http://localhost:5600/person/${updPerson.id}`, updPerson)    
      console.log(response.data)
    },
    async updCompany({ commit },updCompany)
    {
      const response = await axios.put(`http://localhost:5600/company//${updCompany.id}`,updCompany)     
      console.log(response.data)
    },
    async getCountries({commit}){
      const response= await axios.get(" http://localhost:5800/data")
     commit('setCountries', response.data)
    },
    async deleteUser({commit}, del){
      await axios.delete(`http://localhost:5600/person/${del.id}`,del)
      // commit('removeUser ', response.data)
    },
    async deleteCompany({commit}, del){
      await axios.delete(`http://localhost:5600/company/${del.id}`,del)
      // commit('removeUser ', response.data)
    },
  },


  modules: {
  }
})
