import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'primeicons/primeicons.css'
import ToastService from 'primevue/toastservice';
import Avatar from 'primevue/avatar';
import AvatarGroup from 'primevue/avatargroup';
import FileUpload from 'primevue/fileupload';


import PrimeVue from 'primevue/config'
import InputText from 'primevue/inputtext';
import Button from 'primevue/button';
import Checkbox from 'primevue/checkbox';
import Dropdown from 'primevue/dropdown'
import InputNumber from 'primevue/inputnumber';
import SelectButton from 'primevue/selectbutton';
import DataTable from 'primevue/datatable';
import Toast from 'primevue/toast';
import Column from 'primevue/column';
import Dialog from 'primevue/dialog';
import ToggleButton from 'primevue/togglebutton';

const app = createApp(App)
app.use(PrimeVue);
app.use(ToastService);

app.component('InputText', InputText);
app.component('Button',Button)
app.component('Checkbox',Checkbox)
app.component('Dropdown',Dropdown)
app.component('SelectButton',SelectButton)
app.component('InputNumber',InputNumber)
app.component('Toast',Toast)
app.component('DataTable',DataTable)
app.component('Column',Column)
app.component('Avatar',Avatar)
app.component('AvatarGroup',AvatarGroup)
app.component('FileUpload',FileUpload)
app.component('ToggleButton', ToggleButton)
app.component('Dialog',Dialog)


app.use(router).use(store).mount('#app')